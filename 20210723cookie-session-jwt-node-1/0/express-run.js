//js进阶--数据交互--
//加载依赖包
var express = require("express")
var bodyParser = require('body-parser'); //http解析

var app = express();
//跨域处理
var allowCrossDomain = function (req, res, next) {
 res.header('Access-Control-Allow-Origin', '*');
 res.setHeader('Access-Control-Allow-Methods', '*');
 res.setHeader('Access-Control-Allow-Headers', '*');
 next();
};
app.use(allowCrossDomain);

app.use(bodyParser.json()); //解析前端发送的json数据
app.use(bodyParser.urlencoded({extended: true})); //解析前端发表单数据

//定义接口 http://localhost:3000/
app.get("/", function(req, res){
	res.send('index')  //接口返回字符串
})
//启动服务
app.listen(3000, function(){console.log("Server started on port 3000.")});