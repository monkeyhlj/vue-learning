## cookie
- 浏览器访问网页时 自动 带上， 存储5k
- 浏览器访问网页时，会自动带上cookieid,便于服务端识别
- 当用户登录完成后会把返回的sessionid覆盖于cookieid

## session
- 服务端内存中记录的会话状态
- 当用户登录成功后，会在服务端内存中生成session对象并返回sessionid
- 目的： cookieid--sessionid  一一对应

## chrome
- >=80版本时，不再自动带cookie(http), https会带cookie
- 解决： 可以手动在header加上token(jwt)

## jwt(比火车票)
- json web token
- 当登录成功后，生成jwt并返回，保存sessionstorage/localstorage
- 服务端生成jwt会指定一个有效期限，1天，2h
- 发起新请求时，取出jwt放到header中, 服务端验证jwt, 有效则可继续访问
- 服务端不再需要session

## cookie&jwt
- 使用cookie简单，浏览器自带;  jwt需要手动放header
- 需要占用服务端内存空间;  不会占用服务端空间
- 当session失效会话就完了；jwt有效期不受控制

## node
- 为什么后端要用node, 因为它是js
- js运行时环境
- 主要使用express框架， 能够快速搭建后端服务接口

## 启动node服务
- 注意切换目录
- 创建package.json， npm init -y
- 安装依赖包 cnpm i -S express body-parser
- 启动node express-run.js
- 可以把上面的命令写入package.json，然后npm run start
- 停止ctrl+C

## 重构express-run
- 模块化import/export
- es6、babel
- 现在只有一个文件，包装多层套路
- 功能：访问db，记录日志，方便配置, 方便维护
- 支持es6语法需要添加.babelrc, 配置babel及模块化支持
- 添加babel-node,它会在运行时使用.babelrc把代码进行编译
- cnpm i -S babel-cli babel-preset-env babel-plugin-add-module-exports
- 根目录添加入口main.js, 从入口运行
- 需要导出myexpress模块并且把listen方法也要导出到外边去，需要用到promise，与vue相同
- 若不把listen放到main.js，可能main.js没有执行完就开始监听了。
- node-promise: bluebird,node8.util.promisify



vue---axios--->permission--->后端--->return promise--->res.then


