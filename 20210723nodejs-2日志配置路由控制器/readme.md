## 端午节 6.25 周四 不上课
## 节前 6.24 要不要上课，调课28上(周日)，投票

## 日志框架winston
- 系统发布上线，出了bug？保存在文件中日志
- 记录系统运行的日志，包括正常，异常等
- 安装日志框架：cnpm i -S winston  -S保存到package.json文件
- 添加日志框架配置文件app/util/logger.js
- 输出日志默认json格式, 可以使用format选项配置输出格式
- 输出2种方式：控制台(便于开发调试)，文件(发布后调试)
- 新创建日志文件 2个条件:文件达到指定大小  或  按天记录

## 可配置性
- 后端框架需要可配置(信贷系统后端测试环境8081  线上环境8082 改代码)
- 改配置文件 或 环境变量process.env 可以package.json配置(npm run dev)
- cross-env PORT=4000 设置环境变量为4000
- 文件：config/config.js

## 路由
- express完整的web框架，有路由(可类比为vue-router)，映射到指定的接口地址
- 文件命名： 驼峰，逗号分割
- process.cwd() 当前工程目录 D:\vip03\node\2

## 控制器
- 用户控制器，权限控制器，合同控制器
- 控制器就是一个接口的细分， 分类
- 给浏览器返回数据
  res.status(200).send(users)
  res.status(200).json(users)
- 对express-run中接口及参数不熟悉同学，js进阶--数据交互

