// 用户服务层
import models from '../models'  //  ./models等价于./models/index
const User = models.jd_user;

export function findUser(account, pwd) {
    return User.findOne({
        where: {
            account: account,   //左边的名字对应user模型名  右边是参数 
            password: pwd
        }
    })  //findOne--User模型自带的
}