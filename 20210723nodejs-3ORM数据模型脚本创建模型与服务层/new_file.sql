/*
Navicat MySQL Data Transfer

Source Server         : hlj
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : tutor

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2020-04-02 17:08:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `agreement`
-- ----------------------------
DROP TABLE IF EXISTS `agreement`;
CREATE TABLE `agreement` (
  `tname` varchar(10) DEFAULT NULL,
  `tnumber` varchar(11) DEFAULT NULL,
  `uname` varchar(10) DEFAULT NULL,
  `unumber` varchar(11) DEFAULT NULL,
  `uid` varchar(18) DEFAULT NULL,
  `uaddress` varchar(20) DEFAULT NULL,
  `salary` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `otherrequest` varchar(20) DEFAULT NULL,
  `ordertime` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT '待审核'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agreement
-- ----------------------------

-- ----------------------------
-- Table structure for `agreementrefuse`
-- ----------------------------
DROP TABLE IF EXISTS `agreementrefuse`;
CREATE TABLE `agreementrefuse` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tname` varchar(10) DEFAULT NULL,
  `tnumber` varchar(11) DEFAULT NULL,
  `uname` varchar(10) DEFAULT NULL,
  `unumber` varchar(11) DEFAULT NULL,
  `uid` varchar(18) DEFAULT NULL,
  `uaddress` varchar(20) DEFAULT NULL,
  `salary` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `otherrequest` varchar(20) DEFAULT NULL,
  `ordertime` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT '待审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agreementrefuse
-- ----------------------------
INSERT INTO `agreementrefuse` VALUES ('1', '多多老师', '14437321658', '侯丽娟', '18381760064', '511322199801296128', '西华师大二期', '2000-3000', '2月', '无', '2019-11-15', '不通过');
INSERT INTO `agreementrefuse` VALUES ('2', '侯哈哈', '15583042262', '侯丽娟', '18381760064', '511322199812034526', '西华师大二期', '2000-3000', '1月', '无', '2019-12-16', '不通过');
INSERT INTO `agreementrefuse` VALUES ('3', '多多老师', '14437321658', '侯丽娟', '18381760064', '511322199812034526', '西华师大二期', '2000-3000', '1月', '无', '2020-04-02', '不通过');
INSERT INTO `agreementrefuse` VALUES ('4', '多多老师', '14437321658', '王俊凯', '15864238952', '511322199711078906', '营山县', '2000-3000', '1月', '无', '2020-04-02', '不通过');
INSERT INTO `agreementrefuse` VALUES ('5', '侯哈哈老师', '15583042262', '侯丽娟', '18381760064', '511322199812034526', '营山县', '2000-3000', '1月', '无', '2020-04-02', '不通过');

-- ----------------------------
-- Table structure for `agreementstore`
-- ----------------------------
DROP TABLE IF EXISTS `agreementstore`;
CREATE TABLE `agreementstore` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tname` varchar(10) DEFAULT NULL,
  `tnumber` varchar(11) DEFAULT NULL,
  `uname` varchar(10) DEFAULT NULL,
  `unumber` varchar(11) DEFAULT NULL,
  `uid` varchar(18) DEFAULT NULL,
  `uaddress` varchar(20) DEFAULT NULL,
  `salary` varchar(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `otherrequest` varchar(20) DEFAULT NULL,
  `ordertime` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT '待审核',
  `endtime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agreementstore
-- ----------------------------
INSERT INTO `agreementstore` VALUES ('3', '侯哈哈', '15583042262', '王俊凯', '15864238952', '511322199801296128', '西华师大', '3000-4000', '1月', '无', '2019-12-16', '签约成功', '2019-12-17');
INSERT INTO `agreementstore` VALUES ('4', '多多老师', '14437321658', '哈哈猴', '15263666666', '511322199812034526', '西华师大二期', '2000-3000', '2月', '无', '2019-11-16', '签约成功', '2020-03-21');
INSERT INTO `agreementstore` VALUES ('7', '多多老师', '14437321658', '哈哈猴', '15263666666', '511322199812034526', '西华师大二期', '2000-3000', '2月', '无', '2019-11-16', '解除协议', '2020-03-21');
INSERT INTO `agreementstore` VALUES ('8', '侯哈哈', '15583042262', '王俊凯', '15864238952', '511322199801296128', '西华师大', '3000-4000', '1月', '无', '2019-12-16', '解除协议', '2020-03-21');
INSERT INTO `agreementstore` VALUES ('9', '侯哈哈老师', '15583042262', '侯丽娟', '18381760064', '511322199812034526', '南充市顺庆区', '2000-3000', '1月', '无', '2020-03-21', '签约成功', '2020-03-21');
INSERT INTO `agreementstore` VALUES ('10', '侯哈哈老师', '15583042262', '李中秋', '15263666666', '511322199711078906', '西华师大二期', '3000-4000', '2月', '无', '2020-03-21', '签约成功', '2020-04-02');
INSERT INTO `agreementstore` VALUES ('11', '侯哈哈老师', '15583042262', '李中秋', '15263666666', '511322199711078906', '西华师大二期', '3000-4000', '2月', '无', '2020-03-21', '解除协议', '2020-04-02');
INSERT INTO `agreementstore` VALUES ('12', '多多老师', '14437321658', '李中秋', '15263666666', '511322199812034526', '南充市顺庆区', '3000-4000', '1月', '无', '2020-04-02', '签约成功', '2020-04-02');
INSERT INTO `agreementstore` VALUES ('13', '多多老师', '14437321658', '李中秋', '15263666666', '511322199812034526', '南充市顺庆区', '3000-4000', '1月', '无', '2020-04-02', '解除协议', '2020-04-02');
INSERT INTO `agreementstore` VALUES ('14', '侯哈哈老师', '15583042262', '王俊凯', '15864238952', '511322199711078906', '北京市', '3000-4000', '1月', '无', '2020-04-02', '签约成功', '2020-04-02');
INSERT INTO `agreementstore` VALUES ('15', '侯哈哈老师', '15583042262', '王俊凯', '15864238952', '511322199711078906', '北京市', '3000-4000', '1月', '无', '2020-04-02', '解除协议', '2020-04-02');

-- ----------------------------
-- Table structure for `colu`
-- ----------------------------
DROP TABLE IF EXISTS `colu`;
CREATE TABLE `colu` (
  `cid` varchar(3) NOT NULL,
  `ctext` varchar(10) NOT NULL,
  `curl` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of colu
-- ----------------------------
INSERT INTO `colu` VALUES ('1', '权限管理', 'http://localhost:8080/Tutor/SelectColuBM');
INSERT INTO `colu` VALUES ('2', '管理员管理', 'http://localhost:8080/Tutor/SelectAllRole');
INSERT INTO `colu` VALUES ('3', '用户_家长管理', 'http://localhost:8080/Tutor/SelectAllUser');
INSERT INTO `colu` VALUES ('4', '用户_教师管理', 'http://localhost:8080/Tutor/SelectAllTeacher');
INSERT INTO `colu` VALUES ('5', '进行中的', 'http://localhost:8080/Tutor/SelectAllAgree');
INSERT INTO `colu` VALUES ('6', '不通过的', 'http://localhost:8080/Tutor/SelectAllAgreeR');
INSERT INTO `colu` VALUES ('7', '业务存储', 'http://localhost:8080/Tutor/SelectAllAgreeS');

-- ----------------------------
-- Table structure for `manager`
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `mname` varchar(20) NOT NULL,
  `mnumber` varchar(20) NOT NULL,
  `mpsw` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('超级管理员', '15881727579', '1');
INSERT INTO `manager` VALUES ('小关', '17701296128', '1');

-- ----------------------------
-- Table structure for `middle`
-- ----------------------------
DROP TABLE IF EXISTS `middle`;
CREATE TABLE `middle` (
  `mnumber` varchar(11) NOT NULL,
  `cid` varchar(3) NOT NULL,
  `mcid` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of middle
-- ----------------------------
INSERT INTO `middle` VALUES ('15881727579', '1', '158817275791');
INSERT INTO `middle` VALUES ('15881727579', '2', '158817275792');
INSERT INTO `middle` VALUES ('15881727579', '3', '158817275793');
INSERT INTO `middle` VALUES ('15881727579', '4', '158817275794');
INSERT INTO `middle` VALUES ('15881727579', '5', '158817275795');
INSERT INTO `middle` VALUES ('15881727579', '6', '158817275796');
INSERT INTO `middle` VALUES ('15881727579', '7', '158817275797');
INSERT INTO `middle` VALUES ('17701296128', '3', '177012961283');
INSERT INTO `middle` VALUES ('17701296128', '4', '177012961284');
INSERT INTO `middle` VALUES ('17701296128', '5', '177012961285');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `tname` varchar(10) NOT NULL,
  `tnumber` varchar(11) NOT NULL,
  `tpsw` varchar(10) NOT NULL,
  `tsex` varchar(5) DEFAULT NULL,
  `tage` varchar(5) DEFAULT NULL,
  `tmajor` varchar(50) DEFAULT NULL,
  `tschool` varchar(50) DEFAULT NULL,
  `txnum` varchar(20) DEFAULT NULL,
  `tsubject` varchar(100) DEFAULT NULL,
  `state` varchar(5) DEFAULT '未选'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('侯哈哈老师', '15583042262', '123', '女', '20', '计算机科学与技术', '计算机学院', '201613140104', '英语、数学', '未选');
INSERT INTO `teacher` VALUES ('多多老师', '14437321658', '123', '女', '36', '网络工程', '电子信息学院', '201613160114', '语文，英语，数学', '未选');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uname` varchar(10) NOT NULL,
  `unumber` varchar(11) NOT NULL,
  `upsw` varchar(10) NOT NULL,
  `usex` varchar(5) DEFAULT NULL,
  `uage` varchar(5) DEFAULT NULL,
  `cname` varchar(10) DEFAULT NULL,
  `csex` varchar(5) DEFAULT NULL,
  `cage` varchar(5) DEFAULT NULL,
  `cgrade` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('侯丽娟', '18381760064', '123', '女', '20', '侯小梅', '女', '12', '6');
INSERT INTO `user` VALUES ('王俊凯', '15864238952', '123', '女', '30', '王侯', '女', '8', '4');
INSERT INTO `user` VALUES ('李中秋', '15263666666', '111', '男', '36', '李爱国', '女', '10', '5');
