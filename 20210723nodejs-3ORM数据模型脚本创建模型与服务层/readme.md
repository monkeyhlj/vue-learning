## 控制层
- 作用：参数验证，控制接口的调用逻辑
- 流程逻辑比喻：(验证参数--查询数据库--写入redis--生成jwt--返回json)
- 若把上面每一步代码代码都写在控制器，代码冗余

## 服务层
- 数据库的操作抽到服务层
- 生成jwt抽到一个工具箱，其它模块需要直接导入
- 需要导入数据模型，用于操作数据库

## MYSQL
- 版本5.7.xx， 安装mysql数据库
- 导入sql脚本 表名 列名(字段名) 字段类型 数据长度 主键(唯一)
- Navicat Premium 使用工具导入sql，先连接到mysql，创建表初始化数据库

## SQL
- 只需了解简单增删改查
- 麻烦：需要写sql并把返回数据手动封装成json对象

## ORM--sequelize
- object relational mapping 对象---数据库表---映射关系
- 可以通过对象操作数据库，返回的数据也自动封装成对象
- 生成数据模型----与表字段对应的一个sequelize-json对象
- 使用shell脚本生成数据模型---先连接到数据库---生成
- sequelize-auto：自动生成数据模型对象
- 执行脚本：sh sequelize-modal.sh 不能在win的命令行中运行
- 安装：cnpm i -S sequelize@5 mysql2

## 脚本
- 工程自动化中使用的非常多, 作用：一键完成所有工作
- linux---*.sh  win---*.bat java---groovy test---py java---maven/gradle

## 
数据库----excel表格
用工具把sql脚本导入excel中  让excel有表头及初始数据
数据模型----数据表  与excel一一对应的数据表格
服务层需要调用数据模型操作数据(增删改查)
生成模型的文件哪里来的，手写的还是安装的？---在sql定义的

rm -rf node_modules\
cnpm i

