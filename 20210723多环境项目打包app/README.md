## 本周作业
- 信贷项目

## 前端打包
- npm run build 
  -- package.json -- vue-cli-service build
  -- 发布到线上的代码，不便于调试
- 打包发布到测试环境
  --增加打包命令 开发(后端地址)  测试(后端地址)  线上(后端地址)
  --npm run build:dev   --开发调试环境
  --npm run build:prod  --线上

## 打包的代码如何运行
- 不能双击dist/index.html
- 放到http容器运行  serve  tomcat  nginx  iis
- npm i -g serve
- 启动打包后的目录  serve dist  访问：http://localhost:5000

## 打包指定不同的环境变量(开发，测试)
--npm run build:prod跟npm run build的区别是不是有没有压缩的区别？
- 添加环境变量
  开发 在根目录添加.env.dev  dev名称与package.json中的mode对应
  线上 在根目录添加.env.prod
  使用环境变量(process.env.NODE_ENV）
  
## 打包手动配置文件
- vuecli4.x脚手架基于webpack
- vuecli零配置， 不需要零配置可以指定vue.config.js（项目根目录）
- 指定子域配置 publicPath
  如需要在http://www.baidu.com下加一个子域http://www.baidu.com/map
  则需要配置publicPath:'/map/'
  注意：打包后需要在dist中加一层目录map，把所有文件移入map目录 启动serve dist

## 打包压缩，大文件处理
- 开发npm run build:dev 7.11mb
- 线上npm run build:prod 1.8mb
- 少了哪些文件？ 注释， 空行， 压缩， 混淆
- 大文件 900k
- 代码细分：三方vue,axios,elementui;  开发代码
- 把三方的代码移除去, 使用三方提供的cdn资源(免费 收费)
- 找三方包cdn资源， 添加到public/index.html, 把代码中的from vue, vue.use()
  // import Vue from 'vue'
  // Vue.use(ElementUI)
  在vue.config.js配置排除三方包
- 通过减少三方包vue 打包后的js  1.74mb

## gzip进一步压缩
- compression-webpack-plugin
- 配置vue.config.js
- 打包会生成以gz结尾的文件
- 如何看浏览是使用gzip
  浏览器控制台 点开相关js文件
  请求头 Accept-Encoding: gzip, deflate, br
  响应头 Content-Encoding: gzip
  性能最大化

## 打包app
- android ios
- h5运行环境(五大浏览器， 浏览器内核(android ios), 小程序)
- 打包工具hbuildx
  创建一个app项目, 把dist目录文件复制到app目录下
  打开manifest.json, 获取唯一的id
  发行---原生app-云打包--选择android--使用Dcloud老版本证书--打包
  初次打包需要DCloud账号密码
- 打包成功后可以下载
- mumu模拟器运行apk文件
  模拟器右下角---添加apk
- 打包后的dist可以发布到服务器上  也可以打包成app


## 打包部署模式
- hash  打包后dist直接使用http容器运行  本地级线上相同
  路由中有#
  可以刷新页面
- history 打包后通过http容器运行刷新会有404
  路由中没#
  不可以刷新页面
  解决：需要将前端与后端一起部署，由后端负责跳转前端(在讲nodejs说明)
  可脚手架里可以刷新页面