let CompressionWebpackPlugin = require("compression-webpack-plugin")

module.exports = {
    // 打包文件访问的相对目录  独立项目 通过项目根目录访问
    // publicPath: './', // http://localhost:5000
    // 打包到子域下面  http://www.baidu.com/map
    publicPath: '/map/', 
    //需要在dist中加一层目录map，把所有文件移入map目录 启动serve dist
    //开发需要map  线上不需要  map--提供代码映射 方便于在浏览器控制台调试
    productionSourceMap: process.env.NODE_ENV==='production' ? false : true,
    //配置webpack
    configureWebpack: config=>{
        let plugins = [
            new CompressionWebpackPlugin({
                algorithm: 'gzip', //压缩方式
                test: /\.js$|\.css$/, //匹配压缩文件
                threshold: 10240  //对大于10k压缩
            })
        ]
        if (process.env.NODE_ENV === 'production') {
            config.mode = "production"
            // config.plugins --vuecli默认提供很多插件
            config.plugins = [...config.plugins, ...plugins]
        }else{
            config.mode = "development"
        }
        //config----vuecli默认配置
        Object.assign(config, { //object.assign 把右边的复制到左边对象中
            externals: { //打包排除的依赖包
                vue: 'Vue',  //去除vue依赖包
                // axios: 'axios',
                // element: 'element-ui'
            }
        })
    }
}

