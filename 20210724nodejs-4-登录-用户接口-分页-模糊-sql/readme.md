## http状态
200 成功
400 错误的请求
500 服务器错误

## sql
查询
select * from jd_user  查询jd_user表的数据
select * from jd_user where account="admin" and `password`="admin@123"  有条件查询
添加
insert into jd_user(account, password, real_name, reg_time) VALUES ("xxxxx", "11111", "yyyyy", now())
更新(一定要加上约束条件)
update jd_user set account='aaaaaa' where id=5
删除(一定要加上约束条件)
delete from jd_user where id=5

## 用户接口
添加
router.route('/user/create').post(userCtrl.createUser);
修改
router.route('/user/update').post(userCtrl.updateUser);
删除
router.route('/user/delete/:id').post(userCtrl.deleteUser);
查询
router.route('/user/query').get(userCtrl.query);

## 分页接口(模糊)
- 输入参数：pageNo页码(从第1页开始)，pageSize当前页条数(一页显示20条)
- 输出参数：list(20条) rows总条数 分页页码=math.ceil(rows/pageSize)
- 模糊查询  模糊匹配
- select * from jd_user where account like '%test%'
- 查找account字段中包括test字符串的所有数据
-

## 表关联
- 用户表 角色表 用户--角色表
- 若需要查询某个用户有什么角色？
- 一个用户可能有多个角色  一个角色可能被多个用户使用
select u.account,u.`password`, r.role_name
from jd_user u 
left join jd_user_role ur on u.id=ur.user_id
left join jd_role r on r.id = ur.role_id
表关联关系 left join xxx on xxx=xxxx



