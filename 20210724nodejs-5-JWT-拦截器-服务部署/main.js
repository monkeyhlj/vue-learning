import express from './config/myexpress'
import logger from './app/util/logger'
import config from './config/config'

const server = express(); //server.listenAsync  ====  promise
//配置文件处理---取端口号---port
// const port = 3000;
//xxxx
//xxxx
//xxxx
server.listenAsync(config.port).then(()=>{
    logger.info("Server started on port "+config.port) //日志 不会被记录到文件
})
