## nodemon
- 每次修改代码后都需要手动重启服务？
- cnpm i -S nodemon
- nodemon-每次保存时自动重启
- nodemon main.js --exec babel-node 使用babel-node执行

## 拦截器
- 作用：过滤过无效请求，包括没有jwt，无效的jwt等

## jwt
- 比如钥匙
- 其中第一段为 header(头)，第二段为 payload (负载)，第三段为signature(签名)
- header: 定义token类型和签名算法，只做base64
- payload: 保存一些不敏感用户信息，如用户名， 只做base64
- sign: 签名，header+payload+secret(加盐) 一起签名算法加密 生成加密字符串
- secret：相当于自己私有的钥匙(很长的字符串64) secret在服务端(私有)
- 以上知道jwt如何生成， 验证jwt是不是有效(通过secret解密)
- cnpm i -S jsonwebtoken
- 服务端一旦生成jwt token，只能等待它过期

## 项目部署-hash
- windows服务器上部署：
- 前端打包npm run build生成打包目录dist, 把dist目录放入http服务器访问
- npm i -g serve  http服务
- serve dist
- 后端启动 先安装依赖cnpm i 启动npm run start
- 代码上传ftp

- linux服务器：
- 代码上传ftp+xshell  xshell可以在终端上上传文件 yum install 
- 用xhell连接服务器 
- 启动后端 先安装依赖 要创建一个sh文件 添加命令：nohup npm run start &  在后端永久运行
- 添加执行权限 sudo chmod +x start.sh
- 前端部署 解压dist目录，创建一个sh文件 文件内容： nohup serve dist &
- 服务器还需要开放端口


- 常用命令
ls 显示当前目录内容
cd 切换目录 
ps 查看进程
vim 编辑文本


## 项目部署-history
- 打包npm run build
- 安装插件connect-history-api-fallback
- 在myexpress.js中配置插件
  app.use(history());
  app.use(express.static(path.join(__dirname, '../dist'))); //指定页面位置
- 只需要启动一个服务即可 npm run start
- 原理： http://localhost:5003/loan-input/index---重定向http://localhost:5003/---
  系统检查到loan-input/index就跳转到对应的模块了



