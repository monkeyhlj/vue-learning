﻿## webpack与vuecli关系
- vuecli基于webpack建构的，有一些配置还需要直接使用webpack的配置项
- 要完全掌握vuecli，必须掌握webpack
- vuecli的配置项较少，若要使用webpack的配置项，则要用configureWebpack
- vuecli启动项目，实际上启动的是webpack-dev-server

## 为什么要打包？
- 项目分2种： 企业级项目，互联网项目
- 企业级项目：jsp, asp, (写好页面，引入js,css,img)完了直接部署
- 互联网项目：尽量减少加载时间，提高用户体验，需要打包
- 打包：文件压缩(注释，代码压成一行)

## 什么是webpack?
- 打包工具 还有gulp grant
- 从项目入口文件开始，装载转换所有文件，优化处理文件，生成最终发布文件
- 重要概念：入口(main.js)，出口(dist)，装载器loader，plugin.
- 生成package.json npm init -y 
- cnpm i -S webpack webpack-cli(可通过命令行操作)
- 零配置： 没有webpack.config.js依然可以打包
- webpack4可以使用零配置 不用配置文件webpack.config.js
- 若指定了webpack.config.js文件，就不会再使用零配置
- npx: 可以直接运行node_modules中的包
- npx webpack  或  
  package.json中加"dev": "webpack-dev-server"
  npm run dev
- webpack默认可以打包压缩项目，零配置

## webpack-dev-server
- 把前端当作一个http容器访问 前后分离标志
- cnpm i -S webpack-dev-server
- 启动前端服务：npx webpack-dev-server
- 根目录

## 手动配置文件
- webpack: webpack.config.js
- vuecli: vue.config.js

## 把打包后的js文件整合到html中
- 手动添加<script src="./index.js"></script>
- html-webpack-plugin作用：将打包后的js自动添加到html

## 项目发布
- npm i -g serve
- serve dist


