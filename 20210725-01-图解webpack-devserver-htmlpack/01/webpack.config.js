//自定义webpack
let path = require('path'); //path node
let HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {  //node模块化的写法
    entry: "./src/index.js",  //入口
    mode: 'development',  //开发 发布production
    output: {                 //出口
        filename: "index.js", //指定输出名字
        //node __dirname 当前目录
        path: path.resolve(__dirname, "dist")  //指定输出目录
    },
    devServer: {  //web服务配置
        port: 8081,
        open: true,  //服务启动时自动打开默认浏览器
        progress: true, //进度条
        contentBase: './dist'  //http容器的根目录
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html', //模板
            filename: 'index.html',       //打包后模板名字
        })
    ]
}

