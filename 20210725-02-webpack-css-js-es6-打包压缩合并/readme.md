## css文件打包
- css怎么引入？ 是在index.js引入， 因为webpack是以js为入口
- webpack默认不支持css打包，若要支持需要css-loader
- cnpm i -S css-loader
- 配置css-loader, 见webpack.config.js
- css-loader只是装载css文件，不会把装载后的文件整合到html上
- 需要整合到html上，需要style-loader, 作用：生成style

## loader执行顺序
- 从右到左
- 从下到上

## loader与plugin区别
- 装载器loader, 装载并转换(css,less)
- plugin, 文件的处理(压缩，混淆，合并等)

## less
- less-loader 装载并把less语法转css
- cnpm i -S less less-loader

## css抽取
- 把css抽取出成一个单独的文件， style-loader是整合到html
- mini-css-extract-plugin

## js,css压缩处理
- js压缩plugin: terser-webpack-plugin
- css压缩plugin: optimize-css-assets-webpack-plugin
- 默认webpack支持js打包压缩(生产模式)，添加optimize-css-assets-webpack-plugin后,
  webpack的js打包压缩无效。就需要配置terser插件

## 依赖包降级处理
- cnpm i -S xxx 默认安装最新版本
- cnpm i -S xxx@2 表示安装主版本为2的最新版本

## es6打包支持
- babel-loader装载转换器(es6,es7,es8->es5)
- nodejs部分支持es6