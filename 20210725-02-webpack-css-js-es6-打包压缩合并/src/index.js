let a = require('./a')

console.log("hello");
console.log("hello");
console.log("hello");
console.log(a)

import('./index.css');   //import node

require('./b.less')

//es6
let fn = ()=>{
    console.log("es6...")
}
fn()

@Log
class Test{
    constructor(){
        console.log("test class")
    }
}
let t = new Test();

function Log(target){
    console.log(target, "loglog")
}