//自定义webpack
let path = require('path'); //path node
let MiniCssExtractPlugin = require('mini-css-extract-plugin')
let HtmlWebpackPlugin = require("html-webpack-plugin");
let TerserPlugin = require('terser-webpack-plugin');
let OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = {  //node模块化的写法
    optimization: {
        minimizer: [new TerserPlugin({}), new OptimizeCssPlugin({})]
    },
    entry: "./src/index.js",  //入口
    mode: 'development',  //开发 发布production
    output: {                 //出口
        filename: "index.js", //指定输出名字
        //node __dirname 当前目录
        path: path.resolve(__dirname, "dist")  //指定输出目录
    },
    devServer: {  //web服务配置
        port: 8081,
        open: true,  //服务启动时自动打开默认浏览器
        progress: true, //进度条
        contentBase: './dist'  //http容器的根目录
    },
    module: {
        rules: [
            {
                test: /\.js$/,//处理以js文件结尾
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [  //babelrc
                            "@babel/preset-env"
                        ],
                        "plugins": [
                            ["@babel/plugin-proposal-decorators", { "legacy": true }],
                            ["@babel/plugin-proposal-class-properties", { "loose" : true }]
                        ]
                    }
                }
            },
            {
                test: /\.css$/,  //以css结尾的文件
                // use: ['style-loader', 'css-loader']    //装载器loader
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.less$/,
                // use: ['style-loader', 'css-loader', 'less-loader']
                use: [MiniCssExtractPlugin.loader, 
                'css-loader', 
                'less-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html', //模板
            filename: 'index.html',       //打包后模板名字
        }),
        new MiniCssExtractPlugin({
            filename: 'index.css'  //抽出css的文件名
        })
    ]
}

