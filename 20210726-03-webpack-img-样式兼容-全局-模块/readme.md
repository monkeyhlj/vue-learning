## 
- 装载器loader(加载)：
- css-loader, less-loader, babel-loader, style-loader
- MiniCssExtractPlugin.loader
- 插件plugin(加工)：
- optimize-css-assets-webpack-plugin
- terser-webpack-plugin
- html-webpack-plugin
- mini-css-extract-plugin

## 全局变量
- jquery  $  遍历dom
- 工具：underscord  lodash(防抖节流)
- 在每一个组件中都可以使用它 import $ from 'jquery';
- 使用全局变量 可以直接在组件中使用 webpack.ProvidePlugin

- 导入前 打包的文件11.9k  导入后 334k 影响加载速度
- 把三方的包排除去(前面讲vuecli打包)， 需要在html手动添加依赖包

- 把三方的包拆开好处：1.分开加载  2.缓存

## 图片处理(三种方式)
- 在js中加载图片 需要file-loader
  (默认使用es6模块化， 通过配置esModule：false改成commonjs模块化)
- 通过css加载 
- 需要html直接作用<img>  需要html-withimg-loader
  html-withimg-loader需要配合file-loader使用并且不能够使用es6模块化
- 若图片很多，每一张图片都需要一个http请求
- 可以把小图片转成base64, 减少http请求，可以使用url-loader


## 模块化
- node  commonjs
- es6 模块化
  es6模块化有一个default，表示默认对象， import表示导入默认对象

## 样式兼容性
- css3，先实现自己的-webkit-xxxx, 所有的都实现了，可以去掉前缀
- 兼容性插件就是给样式自动添加前缀
- 需要postcss-loader, 在根目录添加postcss.config.js, 
  还要在package.json配置browserslist