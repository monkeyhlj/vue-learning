let a = require('./a')

console.log("hello");
console.log("hello");
console.log("hello");
console.log(a)

import('./index.css');   //import node

require('./b.less')

//es6
let fn = ()=>{
    console.log("es6...")
}
fn()

@Log
class Test{
    constructor(){
        console.log("test class")
    }
}
let t = new Test();

function Log(target){
    console.log(target, "loglog")
}

// import $ from 'jquery';  设置全局变量后
console.log($)

// 图片加载
var jpg = require('./1.jpg');   //file-loader
console.log(jpg)   //jpg不是路径  是require后的module对象
let img = new Image();
img.src = jpg.default;   //图片名字 jpg.default
// document.body.appendChild(img)

//commonjs
var m1 = require('./com')
console.log("m1:", m1.obj.count)
m1.add();
console.log("m1:", m1.obj.count)

//es6
import m2 from './es6'
console.log("m2:", m2.obj.count)
m2.add();
console.log("m2:", m2.obj.count)
