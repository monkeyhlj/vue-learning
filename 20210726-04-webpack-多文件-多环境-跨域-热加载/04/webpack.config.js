let path = require("path")
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: { //配置入口
        home: './src/index.js',
        other: './src/other.js'
    },
    output: { //输出目录   输出多个文件home.js  other.js
        filename: "[name].js",  //动态输出多个文件
        path: path.resolve(__dirname, "dist")
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html', //模板文件
            filename: 'home.html',        //生成的html文件
            chunks: ['home']              //html文件中引用的js
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'other.html',
            chunks: ['home', 'other']
        })
    ]
}
