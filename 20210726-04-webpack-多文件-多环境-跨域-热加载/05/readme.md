## 跨域
- url三部分不同就跨域  (协议 域名 端口)
  http://www.baidu.com  https://www.baidu.com
  http://localhost:8080
- js进阶---数据交互---后端cros,ng(已讲)

## 通过node模拟接口
- 启动node  server.js

## 通过webpack解决跨域
- 通过proxy解决
  前端：http://localhost:8080/user
  后端：http://localhost:3000/user
  1.先要用工具postman测试后端接口是否Ok
  2.前端如何代理到后端  只需把uri部分替换掉

- 前端都有一个前缀 有2种api /web  /mobile
  前端：http://localhost:8080/api/user
  后端：http://localhost:3000/user
  在代理过程中把api替换为空 pathRewrite

- 注意：打包后没有proxy配置了，即proxy只在开发时有效
- 打包后后端不处理跨域，可以nginx代理(在数据交互中已讲)，
  还可以将前后一起打包

- 将前端和后端一起打包
let webpack = require('webpack')
let middle = require('webpack-dev-middleware')
let config = require("./webpack.config")
let compiler = webpack(config)
app.use(middle(compiler))
-启动 node server.js

- vue history  vuecli打包

## 启动前端服务
- npm run dev

## 热加载
- HOT MODULE REPLACEMENT [HMR]
- 默认刷新整个页面
- HMR: 只会局部更新
- 当修改子文件时，不会刷新整页面
