let express = require('express');

let app = express();

// 将前端和后端一起打包
let webpack = require('webpack')
let middle = require('webpack-dev-middleware')
let config = require("./webpack.config")
let compiler = webpack(config)
app.use(middle(compiler))

//pc
app.get('/api/user', (req, res)=>{
    res.json({name:"test1"})
})
//mobile
app.get('/mb/user', (req, res)=>{
    res.json({name:"mobile"})
})

app.listen(3000, ()=>{console.log("服务已启动，端口3000")})