//自定义webpack
let path = require('path'); //path node
let HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require('webpack');

module.exports = {  //node模块化的写法
    entry: "./src/index.js",  //入口
    mode: 'development',  //开发 发布production
    output: {                 //出口
        filename: "index.js", //指定输出名字
        //node __dirname 当前目录
        path: path.resolve(__dirname, "dist")  //指定输出目录
    },
    devServer: {  //web服务配置
        hot: true, //热加载
        port: 8080,
        // progress: true, //进度条
        contentBase: './dist',  //http容器的根目录
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                // pathRewrite: {'/api':''}
            },
            '/mb': {
                target: 'http://localhost:3000',
                // pathRewrite: {'/api':''}
            }
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html', //模板
            filename: 'index.html',       //打包后模板名字
        }),
        new webpack.HotModuleReplacementPlugin()  //热更新插件
    ]
}

