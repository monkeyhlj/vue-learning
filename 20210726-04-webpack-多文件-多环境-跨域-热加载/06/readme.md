## webpack多环境
- 开发 server：http://172.1.10.5:8081
- 测试 server：http://172.1.10.6:8082
- 上线 server：http://172.1.10.7:8081
通过命令简单切换环境打包

## vuecli多环境配置
- 前面已讲(.env  .env.dev)

## 配置不同的打包命令
- 开发打包  npm run build:dev
- 测试打包  npm run build:test
- 上线打包  npm run build:pro
在package.json配置命令, 需要指定配置文件 --config  xxx.js


- 添加基类文件，用于保存公共的配置 webpack.base.js
- 添加子类文件，webpack.dev.js 
- 相同的在父类  不同的在子类

## 定义变量
webpack.DefinePlugin

