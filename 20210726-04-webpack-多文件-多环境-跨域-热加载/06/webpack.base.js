let path = require("path")
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: { //输出目录   输出多个文件home.js  other.js
        filename: "bundle.js",  //动态输出多个文件
        path: path.resolve(__dirname, "dist")
    },
    devServer: {  //web服务配置
        port: 8080,
        progress: true, //进度条
        contentBase: './dist',  //http容器的根目录
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html', //模板文件
            filename: 'index.html',        //生成的html文件
        })
    ]
}
