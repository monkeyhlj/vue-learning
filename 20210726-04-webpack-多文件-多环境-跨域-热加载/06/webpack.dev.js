//继承父配置文件
let {smart} = require('webpack-merge')
let base = require('./webpack.base')
let webpack = require("webpack")

module.exports = smart(base, {
    //子类的配置
    mode: "development",
    plugins: [
        new webpack.DefinePlugin({
            DEV_ENV: JSON.stringify('dev')  //定义变量
        })
    ]
})