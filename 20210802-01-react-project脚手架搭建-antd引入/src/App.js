//import React from 'react';
import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from 'antd';
import 'antd/dist/antd.css'
//导入组件
import Home from './project/index'

//组件定义二种方式  
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>123456
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
// export default App;

//几种不同的定义组件方式
//一、es6箭头函数
// const App =()=> (
//     <div className="App">
//       123456
//     </div>
//   );

// export default App;

//二、es6 class类
// class App extends React.Component {
//   constructor(){
//     super();
//     this.state ={ }
//   }
//   render(){
//     return (
//       <div className="App">
//             99999
//       </div>
//     )
//   }
// }
// export default App;
//二、es6 class类  简洁
class App extends Component {
  render(){
    return (
      <div className="App">
            {/* <Button type="primary">Button</Button> */}
            <Home />
      </div>
    )
  }
}
export default App;