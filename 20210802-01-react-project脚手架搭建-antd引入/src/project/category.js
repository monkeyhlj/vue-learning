import React,{Component} from 'react';

class Category extends Component{
  constructor() {
      super();
      this.state = {
          lists:[
              {title:'在线咨询1',icon:'icon-shenghuo',color:'#f60'},
              {title:'在线咨询2',icon:'icon-shenghuo',color:'#49dacf'},
              {title:'在线咨询3',icon:'icon-jiaoyu',color:'#fa6900'},
              {title:'在线咨询4',icon:'icon-shenghuo',color:'#49dacf'},
              {title:'在线咨询5',icon:'icon-jiajujiafang',color:'#f60'},
              {title:'在线咨询6',icon:'icon-shenghuo',color:'#49dacf'},
              {title:'在线咨询7',icon:'icon-11',color:'#f00'}
          ]
      };
  };
  render () {
      let {lists} = this.state;
      var _list = lists.map((item,i)=>{
          return (
              <div className="category" key={i}>
                  <i className={[`iconfont ${item.icon}`]} style={{background:item.color}}></i>
                  <label>{item.title}</label>
              </div>
          )
      })
      return (
          <div className="index-category">
              {_list}
          </div>
      );
  }
};
  export default Category;