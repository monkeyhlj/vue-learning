import React,{Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'

//导入组件
import Home from './home/index'
import Login from './login/index'

class Routes extends Component {
    render(){
      return (
        <Router>
            <div>
                <Route path='/login' component={Login} ></Route>
                <Route path='/index' component={Home} ></Route>
            </div>
        </Router>
      )
    }
  }
  export default Routes;