import React,{Component} from 'react';
import { Layout, Menu,Button } from 'antd';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import './index.scss';
import Main from './main'
import {MenuData} from '../utils/menu'

const { Header, Sider } = Layout;
const { SubMenu } = Menu;

class Home extends Component {
    constructor(){
      super();
      this.state = {
        collapsed: false,
      };
    }
    toggle = () => {
      this.setState({
        collapsed: !this.state.collapsed,
      });
    };
    goBack(){  //返回
      sessionStorage.removeItem('username'); //清除数据
      //跳转到登录
      this.props.history.push('/login')
    }
    componentDidMount(){
      this.load();
    };
    load(){
      if(sessionStorage.username){
        this.props.history.push('/index')
      }else {
        this.props.history.push('/login')
      }
    }
    render(){
      return (
        <Router>
          <Layout>
            <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
              <div className="logo" />
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                {
                  MenuData.map((item,i)=>{
                    if(item.children && item.children.length){
                      return (
                        <SubMenu key={item.url} icon={<UploadOutlined />} title={item.name}>
                          {
                            item.children.map((sub,i)=>{
                              return (
                                <Menu.Item key={sub.url}>
                                  <Link to={sub.url}>{sub.name}</Link>
                                </Menu.Item>
                              )
                            })
                          }
                          
                        </SubMenu>
                      )
                    }
                    return (
                      <Menu.Item key={item.url} icon={<UserOutlined />}>
                        <Link to={item.url}>{item.name}</Link>
                      </Menu.Item>
                    )
                  })
                }
                
              </Menu>
            </Sider>
            <Layout className="site-layout">
              <Header className="site-layout-background" style={{ padding: 0 }}>
                {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                  className: 'trigger',
                  onClick: this.toggle,
                })}
                <Button type="primary" onClick={()=>this.goBack()}>返回</Button>
              </Header>
             {/* 主体内容 */}
             <Main />
            </Layout>
          </Layout>
        </Router>
      )
    }
  }
  export default Home;