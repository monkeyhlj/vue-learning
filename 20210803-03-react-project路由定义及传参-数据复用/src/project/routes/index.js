import React,{Component} from 'react';

//导入组件
import Banner from '../other/banner'

// 音乐组件
class Music extends Component {
  render(){
    return <h1>Music</h1>
  }
}
//类别组件
class Category extends Component {
  render(){
  return <h1>Category </h1>
  }
}

let routes = [
    {
        path:'/index',
        component:Banner
    },
    {
        path:'/tool',
        component:Category
    },
    {
        path:'/music',
        component:Music
    }
]
export default  routes;