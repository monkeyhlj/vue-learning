import React,{Component} from 'react';
import { Layout, Menu } from 'antd';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import {MenuData} from '../utils/menu'

const { Sider } = Layout;
const { SubMenu } = Menu;

class SiderPage extends Component {
    constructor(){
      super();
      this.state = {

      };
    }
    render(){
      return (
            <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
              <div className="logo" />
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                {
                  MenuData.map((item,i)=>{
                    if(item.children && item.children.length){
                      return (
                        <SubMenu key={item.url} icon={<UploadOutlined />} title={item.name}>
                          {
                            item.children.map((sub,i)=>{
                              return (
                                <Menu.Item key={sub.url}>
                                  <Link to={sub.url}>{sub.name}</Link>
                                </Menu.Item>
                              )
                            })
                          }
                          
                        </SubMenu>
                      )
                    }
                    return (
                      <Menu.Item key={item.url} icon={<UserOutlined />}>
                        <Link to={item.url}>{item.name}</Link>
                      </Menu.Item>
                    )
                  })
                }
              </Menu>
            </Sider>
      )
    }
  }
  export default SiderPage;