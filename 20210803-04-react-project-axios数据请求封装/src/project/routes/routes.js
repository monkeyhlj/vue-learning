import React,{Component} from 'react';
import {BrowserRouter as Router, Route,Redirect} from 'react-router-dom'

//导入组件
import Home from '../home/index'
import Login from '../login/index'

//exact 精确匹配

class Routes extends Component {
    render(){
      return (
        <Router>
            <div>
                <Route path='/login' component={Login} ></Route>
                <Route path='/index' component={Home} ></Route>
                {/* 重定向 */}
                {/* <Redirect from='/' to='/login' ></Redirect> */}
                <Route exact path='/' component={Login} ></Route>
            </div>
        </Router>
      )
    }
  }
  export default Routes;