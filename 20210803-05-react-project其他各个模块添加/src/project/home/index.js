import React,{Component} from 'react';
import { Layout, Menu,Button } from 'antd';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'

import './index.scss';
import SiderPage from './sider'   //侧边栏
import HeaderPage from './header'   //右侧头部内容
import Main from './main'    //右侧主体内容

class Home extends Component {
    constructor(){
      super();
      this.state = {
        collapsed: false,
      };
    }
    toggle = () => {
      this.setState({
        collapsed: !this.state.collapsed,
      });
    };

    componentDidMount(){
      this.load();
    };
    load(){
      if(sessionStorage.username){
        this.props.history.push('/index')
      }else {
        this.props.history.push('/login')
      }
    }
    render(){
      return (
        <Router>
          <Layout>
            {/* 左侧侧边栏 */}
            <SiderPage  collapsed={this.state.collapsed}/>
            <Layout className="site-layout">
              {/* 右侧头部 */}
              <HeaderPage  toggle={this.toggle} collapsed={this.state.collapsed} history={this.props.history}/>
             {/* 主体内容 */}
             <Main />
            </Layout>
          </Layout>
        </Router>
      )
    }
  }
  export default Home;