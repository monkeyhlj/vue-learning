import React,{Component} from 'react';
import { Layout, Menu,Button } from 'antd';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import './index.scss';
import Main from './main'

const { Header, Sider } = Layout;
const { SubMenu } = Menu;

class Home extends Component {
    constructor(){
      super();
      this.state = {
        collapsed: false,
      };
    }
    toggle = () => {
      this.setState({
        collapsed: !this.state.collapsed,
      });
    };
    goBack(){  //返回
      sessionStorage.removeItem('username'); //清除数据
      //跳转到登录
      this.props.history.push('/login')
    }
    componentDidMount(){
      this.load();
    };
    load(){
      if(sessionStorage.username){
        this.props.history.push('/index')
      }else {
        this.props.history.push('/login')
      }
    }
    render(){
      return (
        <Router>
          <Layout>
            <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
              <div className="logo" />
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1" icon={<UserOutlined />}>
                  <Link to='/index'>首页</Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                  <Link to='/tool'>工具模块</Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<UploadOutlined />}>
                  <Link to='/music'>音乐模块</Link>
                </Menu.Item>
                <SubMenu key="sub1" icon={<UploadOutlined />} title="音乐模块 ">
                  <Menu.Item key="5">12345</Menu.Item>
                  <Menu.Item key="6">Option 6</Menu.Item>
                  <Menu.Item key="7">Option 7</Menu.Item>
                  <Menu.Item key="8">Option 8</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Layout className="site-layout">
              <Header className="site-layout-background" style={{ padding: 0 }}>
                {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                  className: 'trigger',
                  onClick: this.toggle,
                })}
                <Button type="primary" onClick={()=>this.goBack()}>返回</Button>
              </Header>
             {/* 主体内容 */}
             <Main />
            </Layout>
          </Layout>
        </Router>
      )
    }
  }
  export default Home;