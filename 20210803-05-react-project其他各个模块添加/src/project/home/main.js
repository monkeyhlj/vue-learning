import React,{Component} from 'react';
import { Layout} from 'antd'
import {Route} from 'react-router-dom'

import HomePage from '../views/home/index'
import Music from '../views/music/index'
import Tools from '../views/tools/index'
import Lists from '../views/lists/index'

const { Content } = Layout;

class Main extends Component {
    render(){
      return (
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
              }}
            >
              <Route path='/index' component={HomePage} ></Route>
              <Route path='/tools' component={Tools} ></Route>
              <Route path='/lists' component={Lists} ></Route>
              <Route path='/music' component={Music} ></Route>
            </Content>
      )
    }
  }
  export default Main;