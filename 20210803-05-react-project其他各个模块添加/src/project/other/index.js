import React,{Component} from 'react';
//import { Button } from 'antd';
//导入组件
import Banner from './banner';
import Category from './category';
//导入字体图标
import '../assets/font/iconfont.css';
import './home.css';

class Home extends Component {
    render(){
      return (
        <div className="index-content">
             <Banner />
             <Category />
        </div>
      )
    }
  }
  export default Home;