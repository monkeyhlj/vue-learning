import React,{Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import routes from './index'

class Routes extends Component {
    constructor(){
      super();
      this.state = {
        n:999
      }
    }
    render(){
      return (
        <Router>
            {/* 跳转 */}
            <ul>
              <li>
                <Link to='/index'>首页</Link>
              </li>
              <li>
                <Link to='/tool'>工具模块</Link>
              </li>
              <li>
                <Link to='/music'>音乐模块</Link>
              </li>
            </ul>
            {/* 渲染出口 */}
            <div>
              {
                routes.map((item,i)=>{
                  return <Route key={i} path={item.path} component={item.component} ></Route>
                })
              }
            </div>
        </Router>
      )
    }
  }
  export default Routes;