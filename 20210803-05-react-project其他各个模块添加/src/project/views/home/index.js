import React,{Component} from 'react';

import ChartPage from './chart'
import {get} from '../../utils/request'

class HomePage extends Component {
  constructor(){
    super();
    this.state ={
      item:[],
      x:['一月','二月','三月','四月','五月','六月']
    }
  }
  componentDidMount(){
    this.getData()
  }
  getData(){
    get('/api/user/chart').then(res=>{
        console.log(res);
       this.setState({item:res.data.data.data});
    })
  }
  render(){
    return <ChartPage {...this.state}/>
  }
}
export default HomePage;