import React,{Component} from 'react';
import { Button ,Table,Modal} from 'antd';

import {get,del} from '../../utils/request'

class Lists extends Component {
  constructor(){
    super();
    this.state={
      lists:[]
    }
  }
  componentDidMount(){
    this.getData()
  }
  getData(){  //获取
    get('/api/user/list').then(res=>{
        console.log(res);
      this.setState({lists:res.data.data});
    })
  }
  delModal(id){  //弹出删除弹框
    Modal.confirm({
      title: '删除管理员',
      content: '确定删除当前管理员？',
      okText:'确定',
      cancelText:'取消',
      onOk:()=>this.delData(id)
    });
  }
  delData(id){  //删除数据
    del('/api/user/delete/'+id).then(res=>{
      console.log(res);
        this.getData() //获取
    })
  }
  render(){
    const columns = [
      {
        title: '用户名',
        dataIndex: 'account',
        key: 'account',
      },
      {
        title: '密码',
        dataIndex: 'password',
        key: 'password',
      },
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render:(text, record) => (
          <a disabled={record.account=='admin'}
             onClick={()=>this.delModal(record.id)}>删除</a>
        )
      },
    ];
    
    return (
      <div>
        <Button type="primary">创建管理者</Button>
        <Table dataSource={this.state.lists} columns={columns} />
      </div>
      
    )
  }
}
export default Lists;