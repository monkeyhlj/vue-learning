import React,{Component} from 'react';
import { Table,Select} from 'antd';

import fetchJsonp  from 'fetch-jsonp'

const { Option } = Select;

class Music extends Component {
  constructor(){
    super();
    this.state={
      lists:[],
      loading:true
    }
  }
  componentDidMount(){
    this.getData(2)
  }
  getData(i){  //获取
    fetchJsonp(`http://tingapi.ting.baidu.com/v1/restserver/ting?xml&calback=&from=webapp_music&method=baidu.ting.billboard.billList&type=${i}&size=100&offset=0&callback=jsonp_1578470847634_65767`,{
      method:'get'
    }).then(res=>{
      return res.json()
    }).then(data=>{
      //console.log(data);
      var arr = [];
      for(var i=0;i<data.song_list.length;i++){
        arr.push({
          title:data.song_list[i].title,
          author:data.song_list[i].author
        })
      };
      console.log(arr);
      this.setState({
        lists:arr,
        loading:false
      })
      
    }).catch(error=>{

    })
  }
  handleChange=value=>{
    console.log(value)
    this.getData(value)
  }
  render(){
    const columns = [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: '歌曲名',
        dataIndex: 'title',
        key: 'title',
      },
      {
        title: '歌手',
        dataIndex: 'author',
        key: 'author',
      },
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      }
    ];
    
    var selectData =[{name:'新歌榜',id:1},{name:'热歌榜',id:2},{name:'摇滚榜',id:11}]
    return (
      <div>
        <Select defaultValue={selectData[0].name} style={{ width: 120 }} onChange={this.handleChange}>
          
          {
            selectData.map(item=>{
              return <Option key={item.id} value={item.id}>{item.name}</Option>
            })
          }
        </Select>
        <Table dataSource={this.state.lists.map((item,i)=>({...item,index:i+1}))} 
            columns={columns} 
            loading={this.state.loading}/>
      </div>
      
    )
  }
}
export default Music;