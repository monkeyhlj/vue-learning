import React,{Component} from 'react';
import { Tabs } from 'antd';

import Salary from './component/salary';
const { TabPane } = Tabs;


class Tools extends Component {
  render(){
    return (
      <div>
        <Tabs defaultActiveKey="1">
          <TabPane tab="工资帽" key="1">
            <Salary />
          </TabPane>
          <TabPane tab="小房租" key="2">
            Content of Tab Pane 2
          </TabPane>
          <TabPane tab="身体健康" key="3">
            Content of Tab Pane 3
          </TabPane>
        </Tabs>
      </div>
    )
  }
}
export default Tools;