import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import 'antd/dist/antd.css'
import Routes from './project/routes/routes'
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './project/redux/reducers/index';

import * as serviceWorker from './serviceWorker';  //资源缓存
const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <Routes  />
  </Provider>,
  document.getElementById('root')
);
//ReactDOM.render(<App />, document.getElementById('root'));
//React.StrictMode严格模式
// ReactDOM.render(
//   <React.StrictMode>
//     <Routes  />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
