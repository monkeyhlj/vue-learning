import React,{Component} from 'react';
import { Layout, Menu,Button } from 'antd';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';


const { Header } = Layout;

class HeaderPage extends Component {
    constructor(){
      super();
      this.state = {

      };
    }
    goBack(){  //返回
      sessionStorage.removeItem('username'); //清除数据
      //跳转到登录
      this.props.history.push('/login')
    }
    render(){
      return (
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.props.toggle,
            })}
            <Button type="primary" onClick={()=>this.goBack()}>返回</Button>
          </Header>
      )
    }
  }
  export default HeaderPage;