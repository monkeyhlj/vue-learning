import React,{Component} from 'react';
import { Layout} from 'antd'
import {Route} from 'react-router-dom'

import HomePage from '../views/home/index'
import Music from '../views/music/index'
import Tools from '../views/tools/index'
import Lists from '../views/lists/index'
import Album from '../views/album/index' 
import Editor from '../views/editor/index' 
import TodoList from '../views/todoList/index'

const { Content } = Layout;

class Main extends Component {
    render(){
      return (
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
              }}
            >
              
              <Route exact path='/index/index' component={HomePage} ></Route>
              <Route path='/index/tools' component={Tools} ></Route>
              <Route path='/index/lists' component={Lists} ></Route>
              <Route path='/index/music' component={Music} ></Route>
              <Route path="/index/editor" component={Editor}></Route>
              <Route path="/index/album" component={Album}></Route>
              <Route path="/index/todoList" component={TodoList} />
            </Content>
      )
    }
  }
  export default Main;