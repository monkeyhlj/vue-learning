import React,{Component} from 'react';
import { Form, Input, Button, notification} from 'antd';
import './index.scss';
import {post} from '../utils/request'

class Login extends Component {
    onFinish = values => {  //提交表单 且成功后的回调事件
      console.log('Success:', values);
      let {username,password} = values;
     post('/api/user/login',{account:username,password:password})
        .then(res=>{
          console.log(res);
          let {code} = res.data;
          if(code == '20000'){
             //保存至本地存储
              sessionStorage.setItem('username',username);
              this.props.history.push('/index/index');
          }else {
            //提示信息
            this.openNotification()
          }
        })
        .catch(error=>{
            //提示信息
            this.openNotification()
        })
    };

    onFinishFailed(errorInfo){ //提交表单 失败后的回调事件
      console.log('Failed:', errorInfo);
      //提示信息
      this.openNotification()
    };
    openNotification = () => {  //消息提示框
      notification.open({
        message: '用户名&密码',
        description:'都是：123',
        duration:2 ,  //默认 4.5 秒后自动关闭，配置为 null 则不自动关闭
        style:{  //内联样式
          width:300
        }
      });
    };
    render(){
      const layout = {   
        labelCol: {  //label 标签布局   栅格
          span: 6,
        },
        wrapperCol: {  //表单设置布局样式
          span: 18,
        },
      };
      const tailLayout = {
        wrapperCol: {
          offset: 8,  //间隔
          span: 16,
        },
      };
      return (
        <Form
            {...layout}
            name="basic"
            initialValues={{   //设置默认值
              username: 'admin',
              password:'admin@123'
            }}
            onFinish={this.onFinish}
            onFinishFailed={()=>this.onFinishFailed()}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[   //校验
                {
                  required: true,   //是否必填
                  message: '请输入用户名！！！',   //错误提示
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: '请输入密码！！！',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
      )
    }
  }
  export default Login;

  