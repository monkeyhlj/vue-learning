import React,{Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'

//导入组件
import Banner from '../other/banner'

// 音乐组件
class Music extends Component {
  render(){
    return <h1>Music {this.props.location.query}</h1>
  }
}
//类别组件
class Category extends Component {
  render(){
  return <h1>Category {this.props.match.params.id}</h1>
  }
}


class Routes extends Component {
    constructor(){
      super();
      this.state = {
        n:999
      }
    }
    render(){
      return (
        <Router>
            {/* 跳转 */}
            <ul>
              <li>
                <Link to='/index/1/10'>首页</Link>
              </li>
              <li>
                <Link to={'/tool/'+this.state.n}>工具模块</Link>
              </li>
              <li>
                <Link to={{pathname:'/music',query:'123456'}}>音乐模块</Link>
              </li>
            </ul>
            {/* 渲染出口 */}
            <div>
                <Route path='/tool/:id' component={Category} ></Route>
                <Route path='/index/:id/:size' component={Banner} ></Route>
                <Route path='/music' component={Music} ></Route>
            </div>
        </Router>
      )
    }
  }
  export default Routes;