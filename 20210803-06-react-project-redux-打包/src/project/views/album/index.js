import React, { Component } from 'react';
import { Row, Col, Card, BackTop,Collapse } from 'antd';
const Panel = Collapse.Panel;

class Album extends Component {
  render() {
        const imgs = [
            [
                require('../../../assets/images/0.jpg'),
                require('../../../assets/images/1.jpg'),
                require('../../../assets/images/2.jpg'),
                require('../../../assets/images/3.jpg'),
                require('../../../assets/images/4.jpg')
            ],
            [
                require('../../../assets/images/5.jpg'),
                require('../../../assets/images/6.jpg'),
                require('../../../assets/images/7.jpg'),
                require('../../../assets/images/8.jpg'),
                require('../../../assets/images/9.jpg')
            ],
            [
                require('../../../assets/images/10.jpg'),
                require('../../../assets/images/11.jpg'),
                require('../../../assets/images/12.jpg'),
                require('../../../assets/images/13.jpg'),
                require('../../../assets/images/14.jpg')
            ],
            [
                require('../../../assets/images/3.jpg'),
                require('../../../assets/images/4.jpg'),
                require('../../../assets/images/5.jpg'),
                require('../../../assets/images/6.jpg'),
                require('../../../assets/images/7.jpg')
            ],
            [
                require('../../../assets/images/6.jpg'),
                require('../../../assets/images/7.jpg'),
                require('../../../assets/images/10.jpg'),
                require('../../../assets/images/13.jpg'),
                require('../../../assets/images/14.jpg')
            ]
        ];
        const imgsTag = imgs.map(v1 => (
            v1.map(v2 => (
                <div className="cloud-box">
                    <Card bordered={true} bodyStyle={{ padding: 0 }}>
                        <div>
                            <img alt="example" width="100%" src={v2} />
                        </div>
                        <div className="pa-m">
                            <h3>React Admin</h3>
                            <small><a target="_blank">https://123.com</a></small>
                        </div>
                    </Card>
                </div>
            ))
        ));
        return (
            <div>
                <Row gutter={10}>
                    <Col span={5}>
                        {imgsTag[0]}
                    </Col>
                    <Col span={5}>
                        {imgsTag[1]}
                    </Col>
                    <Col span={5}>
                        {imgsTag[2]}
                    </Col>
                    <Col span={5}>
                        {imgsTag[3]}
                    </Col>
                    <Col span={4}>
                        {imgsTag[4]}
                    </Col>
                </Row>

                <Collapse defaultActiveKey={['1']} >
                    <Panel header="This is panel header 1" key="1">
                      <p>1234</p>
                    </Panel>
                    <Panel header="This is panel header 2" key="2">
                      <p>234t</p>
                    </Panel>
                    <Panel header="This is panel header 3" key="3" disabled>
                      <p>2345t</p>
                    </Panel>
                  </Collapse>

                <BackTop visibilityHeight='300' target={()=>window} />
                <div>1234ty345</div>
            </div>
        )
    }
}

export default Album;
