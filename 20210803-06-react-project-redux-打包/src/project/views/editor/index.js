import React, { Component } from 'react';
import { Row, Col, Card } from 'antd';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {EditorState} from 'draft-js';
import './index.scss'


export default class wysiwyg extends Component {
    constructor(props) {
        super();
        this.state = {
            editorState: EditorState.createEmpty(),
        };
    }
    //编辑状态改变(针对编辑器)
    onEditorStateChange= (editorState) => {
        this.setState({
        editorState:editorState
        });
    };
    render() {
        const { editorState } = this.state;
        return (
            <div>
                <Row>
                    <Col span={24}>
                        <div className="cloud-box">
                            <Card title="富文本编辑器" bordered={true} >
                                <Editor
                                    editorState={editorState}
                                    editorClassName="home-editor123"
                                    //编辑状态改变
                                    onEditorStateChange={this.onEditorStateChange}
                                    toolbar={{
                                        history: { inDropdown: true },
                                        list: { inDropdown: true },
                                        textAlign: { inDropdown: true },
                                        fontFamily: { options: ['宋体', '黑体', '楷体', '微软雅黑','Arial',]}
                                    }}
                                />
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}