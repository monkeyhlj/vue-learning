import React,{Component} from 'react';
import echarts from 'echarts';
import ReactEcharts from "echarts-for-react";

class ChartPage extends Component {
  render(){
    var option = {
        xAxis: {
          type: 'category',
          data: this.props.x
        },
        yAxis: {
          type: 'value'
        },
        series: [{
          data: this.props.item,
          type: 'line'
        }]
    };
    return (
      <ReactEcharts
        option={option}
        style={{width:'100%',height:'300px'}}
      />
    )
  }
}
export default ChartPage;