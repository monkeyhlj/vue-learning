import React,{Component} from 'react';
import { Form, Input} from 'antd';

class MusicPage extends Component { 
  render(){
    return (
        <Form onSubmit={this.handleSubmit}>
            <Form.Item
                    label="Username"
                    name="username"
                    rules={[   //格式校验
                    {
                        required: true,
                        message: 'Please input your username!',
                    },
                    ]}
                >
                    <Input />
                </Form.Item>
           
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                    ]}
                >
                    <Input.Password />
                </Form.Item>
        </Form>
    );
  }
}
export default MusicPage;


