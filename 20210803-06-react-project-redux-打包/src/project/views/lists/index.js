import React,{Component,useState} from 'react';
// import { Button ,Table,Modal,Form, Input} from 'antd';
import { Button, Modal, Form, Input, Table } from 'antd';


import {get,del,post} from '../../utils/request'
import Add from './add'

class Lists extends Component {
  
  constructor(){
    super();
    this.state={
      lists:[],
      visible:false,
      password:'',
      username:'',
    }
  }
  componentDidMount(){
    this.getData()
  }
  getData(){  //获取
    get('/api/user/list').then(res=>{
        console.log(res);
      this.setState({lists:res.data.data});
    })
  }
  delModal(id){  //弹出删除弹框
    Modal.confirm({
      title: '删除管理员',
      content: '确定删除当前管理员？',
      okText:'确定',
      cancelText:'取消',
      onOk:()=>this.delData(id)
    });
  }
  delData(id){  //删除数据
    del('/api/user/delete/'+id).then(res=>{
      console.log(res);
        this.getData() //获取
    })
  }
  showModal = () => {  //弹层显示
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    console.log(this.state.username)
    const {username,password} = this.state;
    post('/api/user/create',{account:username,password:password})
    .then(res=>{
      console.log(res);
      if(res.data.code=='20000'){
        this.getData(); //调用查询数据接口
        this.setState({
          visible: false  //弹层隐藏
        });
      }else {}
    })
    .catch(error=>{
      console.log(error)
    })
  }
  handleCancel = e => {  //点击取消
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleChange(e){
    var name=e.target.name;   //name  password  phone
    this.setState({
      [name]:e.target.value
    })
  }
  render(){
    const columns = [
      {
        title: '用户名',
        dataIndex: 'account',
        key: 'account',
      },
      {
        title: '密码',
        dataIndex: 'password',
        key: 'password',
      },
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render:(text, record) => (
          <a disabled={record.account=='admin'}
             onClick={()=>this.delModal(record.id)}>删除</a>
        )
      },
    ];

    return (
      <div>
        <Button type="primary"  onClick={this.showModal}>创建管理者</Button>
        <Table dataSource={this.state.lists} columns={columns} />
        <Modal
            title="创建登录者"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            okText='创建'
            cancelText = '取消'
          >
            <div>
            <label>用户名：</label>
            <input className="ant-input" type="text" value={this.state.username} name="username"
                              onChange={(event)=>this.handleChange(event)}/>
            </div>
            <div>
            <label>密码：</label>
            <input className="ant-input" type="password" value={this.state.password} name="password"
                              onChange={(event)=>this.handleChange(event)}/>
            </div>

          </Modal>
      </div>
      
    )
  }
}
export default Lists;