import React from 'react'
import { connect } from 'react-redux'
import { add,del,addTodo,delTodo,toggleTodo,setVisibi} from '../../redux/actions/todoList'
import './index.scss'

//@connect 连接  修饰器 
@connect(
    (state) => ({
        num:state.num,
        todoList:state.todoList,  //任务列表
        setVisibi:state.setVisibi
    })
) 

class TodoList extends React.Component {
    add2(){
        this.props.dispatch(add({type:'ADD',n:this.input.value}))     //this.props ==== store
    }
    del2(){
        this.props.dispatch(del({type:'DEL',n:this.input.value}))     //this.props ==== store
    }
    addTodo2(){  //添加任务列表
        var _id = Math.max(...this.props.todoList.map(item=>item.id))+1;
        this.props.dispatch(addTodo({
            type:'ADD_TODO',
            id:_id ,
            name:this.input.value,
            check:false
        }))  
    }
    delTodo2(id){  //删除任务列表
        this.props.dispatch(delTodo({
            type:'DEL_TODO',
            id:id 
        }))  
    }
    toggleTodo2(id){  //复选框切换
        this.props.dispatch(toggleTodo({
            type:'TOGGLE_TODO',
            id:id 
        })) 
    }
    handleFilter(type){//切换任务
        this.props.dispatch(setVisibi({type:'SET_VISIBI',filter:type}));
    }
    render() {
        let {num,todoList,setVisibi} = this.props;   //从store获取值
        var data = todoList;
        if(setVisibi.filter=='NULL'){
            data = todoList.filter(v=>!v.check)
        }else if(setVisibi.filter=='COM'){
            data = todoList.filter(v=>v.check)
        }
        return (
            <div> 
                <h1>{num}</h1>
                <button type="submit" onClick={()=>this.add2()}>+</button> 
                <button type="submit" onClick={()=>this.del2()}>-</button> 

                <input type="text" placeholder="添加任务" ref={dom=>this.input = dom} />
                
               <div className="todo-tab">
               <div className="todo-tab_item">
                        <a style={{color:setVisibi.filter =='ALL'?'#f01414':'#4d555d'}} 
                        onClick={()=>this.handleFilter('ALL')}>全部任务</a>
                    </div>
                    <div className="todo-tab_item">
                        <a style={{color:setVisibi.filter =='NULL'?'#f01414':'#4d555d'}} 
                        onClick={()=>this.handleFilter('NULL')}>待办任务</a>
                    </div>
                    <div className="todo-tab_item">
                        <a style={{color:setVisibi.filter =='COM'?'#f01414':'#4d555d'}} 
                        onClick={()=>this.handleFilter('COM')}>已完成任务</a>
                    </div>
                </div>
                <ul className="list-group">
                    {
                        data.map(item=>{
                            return (
                                <li key={item.id} className="todo-list_li" 
                                style={{'textDecoration':item.check?"line-through":"none"}}>
                                    <input type="checkbox" className="check-box" 
                                        checked={item.check}
                                        onClick={()=>this.toggleTodo2(item.id)} />
                                    {item.name}
                                    <button className="todo-list_del" 
                                         onClick={()=>this.delTodo2(item.id)}>删除</button>
                                </li>
                            )
                        })
                    }
                    
                </ul> 
                <button type="submit" className="todo-btn" onClick={()=>this.addTodo2()}>添加任务</button>    
            </div>
        )
    }
}
export default TodoList;