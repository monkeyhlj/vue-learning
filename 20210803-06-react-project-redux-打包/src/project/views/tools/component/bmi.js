import React from 'react'
import {Row,Col,Input,message,Button,Form,Card} from 'antd'

  const tailLayout = {
    wrapperCol: {
      offset: 4,
	  span: 16,
	  offset: 4,
    },
  };

class Bmi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
			height:'',//身高
			weight:'',//体重
			bmi:'',    //BMI
            level:''    //等级
        }
    }
	//公共方法
    handleChange2 = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        this.setState({[name]:value});     
    }
	//重置
	handleReset2(e){
        //e.preventDefault();
        this.setState({height:'',weight:'',bmi:'',level:''});
    }
	//提交
    handleSubmit2 = (e) => {
        //e.preventDefault();
		console.log(123);
		let {height,weight} = this.state;
		if (!height || !weight) {
			message.success('请输入身高/体重！');
			return;
		}
		let bmi = (weight/((height/100)**2)).toFixed(1);
		let level;
		if(bmi<18.5) {
			level = 0;
		} else if(bmi >= 18.5 && bmi <= 24.9) {
			level = 1;
		} else if(bmi === 25) {
			level = 2;
		} else if(bmi >= 25 && bmi <= 29.9) {
			level = 3;
		} else if(bmi >= 30) {
			level = 4;
		}
		this.setState({bmi:bmi,level:level});
    }
    render(){
		let i = this.state.level;
        return (
            <div>
                <Form
					{...tailLayout}
					name="basic"
					initialValues={{  //表单默认值，只有初始化以及重置时生效

					}}
					onFinish={this.onFinish}
					onFinishFailed={this.onFinishFailed}
				>
                <Form.Item>
					<Input type="number" addonBefore="您的体重：" name="weight" addonAfter="kg" value={this.state.weight}
                         onChange={(event) => this.handleChange2(event)}/>
                </Form.Item>
				<Form.Item>
					<Input type="number" addonBefore="您的身高：" addonAfter="cm" name="height"  value={this.state.height} 
						onChange={this.handleChange2}/>
                </Form.Item>
				<Form.Item >
					<Button style={{width:'50%'}} htmlType="submit" onClick={this.handleSubmit2}>计算</Button>
					<Button style={{width:'50%'}} onClick={(event) => this.handleReset2(event)}>重置</Button>
                </Form.Item>
				<Form.Item >
					<Input addonBefore="您的BMI：" value={this.state.bmi} id="red" disabled/>
                </Form.Item>
				<Form.Item >
					<Card bodyStyle={{ padding:0}} id="cardWrap">
  	  					    <p className={i===0?'activeP':'die'}>偏瘦{'<18.5'}</p>
  	  					    <p className={i===1?'activeP':'die'}>正常{'18.5～24.9'}</p>
  	  					    <p className={i===2?'activeP':'die'}>超重{'=25'}</p>
  	  					    <p className={i===3?'activeP':'die'}>偏胖{'25.0～29.9'}</p>
  	  					    <p className={i===4?'activeP':'die'}>肥胖{'30.0～34.9'}</p>
  	  					</Card>
                </Form.Item>

				</Form>
			</div>
        )
    }
}
export default Bmi;