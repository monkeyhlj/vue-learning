import React,{Component} from 'react';
import { Tabs } from 'antd';

import Salary from './component/salary';
import House from './component/house'
import Bmi from './component/bmi'
import './index.scss'
const { TabPane } = Tabs;


class Tools extends Component {
  render(){
    return (
      <div>
        <Tabs defaultActiveKey="1">
          <TabPane tab="工资帽" key="1">
            <Salary />
          </TabPane>
          <TabPane tab="小房租" key="2"><House /></TabPane>
          <TabPane tab="身体指数" key="3"><Bmi /></TabPane>
        </Tabs>
      </div>
    )
  }
}
export default Tools;