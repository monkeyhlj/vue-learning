let ws = require('ws');            //引入websocket模块
let uuid = require('uuid');        //引入创建唯一id模块
let socketServer = ws.Server;
let clientIndex = 0;
let wss = new socketServer({port: 8090}, ()=>{
	console.log("服务启动，端口8090.")
});    //创建websocketServer实例监听8090端口

let clients = [];                //创建客户端列表，用于保存客户端及相关连接信息

/**
 * 广播所有客户端消息
 * @param  {String} type     广播方式(admin为系统消息，user为用户消息)
 * @param  {String} message  消息
 * @param  {String} nickname 用户昵称，广播方式为admin时可以不存在
 */
function broadcastSend(type, message, nickname) {
	console.log("type:"+type)
    clients.forEach(function(v, i) {
        if(v.ws.readyState === ws.OPEN) {
            v.ws.send(JSON.stringify({
                "type": type,
                "nickname": nickname,
                "message": message
            }));
        }
    })
}

//定时发送图表数据
function echartData() {
    function sortNumber(a,b){
        return a-b;//升序
    }
    //测试数据
    var iArray = [];
    function getRamdon(start, end) {
        var temp = end - start + 1;   //91
        return Math.abs(Math.floor(Math.random()*temp)) + start;   //从start开始
    }
    for(var i=0; i < 10; i++) {
        iArray.push(getRamdon(100,1000))   //循环10次，添加10个数在数组里
    }
    iArray.sort(sortNumber);
    return iArray;
}

var inter = null;
function echartSend() {
	//第一次，以后每5秒更新一次
	var arr = echartData();
	broadcastSend("echart", arr, "system");
	if (inter) clearInterval(inter);
    inter = setInterval(function() {
        var arr = echartData();
        broadcastSend("echart", arr, "system");
    }, 5000)
}

//监听连接
wss.on('connection', function(ws) {
    let client_uuid = uuid.v4();
    let nickname = `AnonymousUser${clientIndex++}`;
    clients.push({
        "id": client_uuid,
        "ws": ws,
        "nickname": nickname
    });
    //发送图表数据
    echartSend();
    console.log(`client ${client_uuid} connected`);
    /**
     * 关闭服务，从客户端监听列表删除
     */
    function closeSocket() {
        for(let i = 0; i < clients.length; i++) {
          if(clients[i].id == client_uuid) {
            let disconnect_message = `${nickname} has disconnected`;
            broadcastSend("notification", disconnect_message, nickname);
            clients.splice(i, 1);
          }
        }
		//无客户端时停止定时器
		if (clients.length == 0){
			console.log("无客户端连接，清除定时器。")
			if (inter) clearInterval(inter);
		}
    }
    /*监听消息*/
    ws.on('message', function(message) {
        if(message.indexOf('/nick') === 0) {
            let nickname_array = message.split('_');
            if(nickname_array.length >= 2) {
                let old_nickname = nickname;
                nickname = nickname_array[1];
                let nickname_message = `Client ${old_nickname} change to ${nickname}`;
                broadcastSend("nick_update", nickname_message, nickname);
            }
        } else {
            broadcastSend("message", message, nickname);
        }
    });
    /*监听断开连接*/
    ws.on('close', function() {
        closeSocket();
    })
})