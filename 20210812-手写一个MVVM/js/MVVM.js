class MVVM {
    constructor(options) {
        // 先把 el 和 data 挂在 MVVM 实例上
        this.$el = options.el;
        this.$data = options.data;

        if (this.$el) {
            // $data数据劫持
            new Observer(this.$data);

            // 将数据代理到实例上 vm.message = "hello"
            this.proxyData(this.$data);

            // 用数据和元素进行编译
            new Compile(this.$el, this);
        }
    }
    proxyData(data) { // 代理数据的方法
        Object.keys(data).forEach(key => {
            Object.defineProperty(this, key, {
                get() {
                    return data[key];
                },
                set(newVal) {
                    data[key] = newVal;
                }
            });
        });
    }
}