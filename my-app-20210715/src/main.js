import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './assets/font/iconfont.css'

Vue.config.productionTip = false
Vue.prototype.$http = axios;
axios.defaults.baseURL = 'http://47.96.117.121:7002/';

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
