import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    redirect:'/home'
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/home/index.vue')
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/detail/index.vue')
  },
  {
    path: '/find',
    name: 'find',
    component: () => import('../views/find/index.vue')
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('../views/order/index.vue')
  },
  {
    path: '/mine',
    name: 'mine',
    component: () => import('../views/mine/index.vue')
  },
]

const router = new VueRouter({
  linkActiveClass:'active',
  routes
})

export default router
