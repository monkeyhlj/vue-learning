import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    isNavShow:true,  //底部导航显示
    cartNum:0,  //购物车数量
    cartLists:[], //购物车数据
};
const getters={  //store对象中的计算属性
  cartNum(state){
    return state.cartNum
  },
  cartLists(state){
    return state.cartLists
  },
};
const actions = { 
  NAVSHOW({commit,state}){  //context就是$store
    commit('NAVSHOW');
  },
  HIDESHOW({commit,state}){  //context就是$store
    commit('HIDESHOW');
  },
  CARTADD({commit,state}){  //购物车数量+
    commit('CARTADD');
  },
  CARTDEL({commit,state}){  //购物车数量-
    commit('CARTDEL');
  },
  CARTLISTSADD({commit,state},data){  //购物车数据+
    commit('CARTLISTSADD',data);
  },
};

const mutations = { 
  NAVSHOW(state){    //显示
    state.isNavShow = true;
  },
  HIDESHOW(state){
    state.isNavShow = false;  //隐藏
  },
  CARTADD(state){    //购物车状态+
    state.cartNum+=1;
  },
  CARTDEL(state){
    state.cartNum-=1;  //购物车状态-
  },
  CARTLISTSADD(state,data){    //购物车数据+
    if(state.cartLists.length >0){
      for(var i=0;i<state.cartLists.length;i++){
        if(state.cartLists[i].product_id == data.product_id){
          state.cartLists[i].count+=1;
          return;
        }
      };
      state.cartLists.push(data);
    }else {
      state.cartLists.push(data);
    }
    
    
  },
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

// export default new Vuex.Store({
//   state: {   //数据
//     num:100,
//     flag:true
//   },
//   mutations: {  //改变state的值
//     ADD(state){
//       state.num+=1;
//     },
//     DEL(state){
//       state.num-=1;
//     }
//   },
//   actions: {   //管理或提交mutations
//     // DEL(context){  //context就是$store
//     //   context.commit('DEL');
//     // }
//     DEL({commit,state}){  //context就是$store
//       commit('DEL');
//     }
//   }
// })
