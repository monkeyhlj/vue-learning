import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    num:100,  
    flag:true
};
const getters={  //store对象中的计算属性
    a(state){
      return state.num % 2 ==0 ?'偶数':'奇数'
    },
    b(state){
      return state.num +=10
    },
};
const actions = { 
  ADD({commit,state}){  //context就是$store
    commit('ADD');
  },
  DEL({commit,state}){  //context就是$store
    commit('DEL');
  }
};

const mutations = { 
    ADD(state){
      state.num+=1;
    },
    DEL(state){
      state.num-=1;
    }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

// export default new Vuex.Store({
//   state: {   //数据
//     num:100,
//     flag:true
//   },
//   mutations: {  //改变state的值
//     ADD(state){
//       state.num+=1;
//     },
//     DEL(state){
//       state.num-=1;
//     }
//   },
//   actions: {   //管理或提交mutations
//     // DEL(context){  //context就是$store
//     //   context.commit('DEL');
//     // }
//     DEL({commit,state}){  //context就是$store
//       commit('DEL');
//     }
//   }
// })
