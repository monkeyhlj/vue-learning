import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Banner.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',  //路径
    name: 'Home',
    component: Home
  },
  {
    path: '/home',  //路径
    component: ()=>import('../views/Banner.vue')  //匹配的组件
  },
  {
    path: '/list',  //路径
    component: ()=>import('../views/List.vue')  //匹配的组件
  },
  {
    path: '/toSon',  //父-子
    component: ()=>import('../views/FtoS/index.vue')  //匹配的组件
  },
  {
    path: '/toFather',  //子-父
    component: ()=>import('../views/StoF/index.vue')  //匹配的组件
  },
  {
    path: '/actionSon',  //父操作子
    component: ()=>import('../views/FactionS/index.vue')  //匹配的组件
  },
  {
    path: '/brother',  //兄弟
    component: ()=>import('../views/Brother/index.vue')  //匹配的组件
  },
  {
    path: '/shuttle',  //穿梭框
    component: ()=>import('../views/Shuttle/index.vue')  //匹配的组件
  }
  ,
  {
    path: '/component',  //动态组件
    meta:{   //自定义属性
      keepAlive:true   //keepAlive变量
    },
    component: ()=>import('../views/component/index.vue')  //匹配的组件
  }
]

const router = new VueRouter({
  linkActiveClass:'selected',    //默认路由样式
  routes
})

export default router
