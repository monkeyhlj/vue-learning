import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',  //路径
    name: 'Home',
    component: Home
  },
  {
    path: '/home',  //路径
    component: ()=>import('../views/Banner.vue')  //匹配的组件
  },
  {
    path: '/list',  //路径
    component: ()=>import('../views/List.vue')  //匹配的组件
  },
  {
    path: '/table',  //路径
    component: Home  //匹配的组件
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  linkActiveClass:'selected',    //默认路由样式
  routes
})

export default router
