import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Banner.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',  //路径
    name: 'Home',
    component: Home
  },
  {
    path: '/part1/:id',  //模块一
    name:'part1',
    component: ()=>import('../views/Part/part1.vue')  //匹配的组件
  },
  {
    path: '/part2',  //模块二
    component: ()=>import('../views/Part/part2.vue')  //匹配的组件
  },
  {
    path: '/home',  //路径
    component: ()=>import('../views/Banner.vue')  //匹配的组件
  },
  {
    path: '/list',  //路径
    name:'list',
    component: ()=>import('../views/List.vue'), //匹配的组件
    children:[
      {
        path: '/list/info',  //路径
        //path:'info',  //二级路由
        name:'listInfo',
        component: ()=>import('../views/Banner.vue'), //匹配的组件
      }
    ]
  },
  {
    path: '/table',  //表格组件
    name:'table',
    component: ()=>import('../views/Table/index.vue'), //匹配的组件
    children:[
      {
        path: '/table/testa',  //路径
        //path:'info',  //二级路由
        name:'tableTesta',
        component: ()=>import('../views/Table/testA.vue'), //匹配的组件
      },
      {
        path:'testb',  //二级路由
        name:'tableTestb',
        component: ()=>import('../views/Table/testB.vue'), //匹配的组件
      }
    ]
  },
]

const router = new VueRouter({
  linkActiveClass:'selected',    //默认路由样式
  routes
})

export default router
